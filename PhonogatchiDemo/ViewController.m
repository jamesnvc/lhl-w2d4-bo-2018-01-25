//
//  ViewController.m
//  PhonogatchiDemo
//
//  Created by James Cash on 25-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic,weak) IBOutlet UIImageView *petImageView;

@property (nonatomic,strong) NSTimer *timer;

@property (nonatomic,strong) UIView *movingView;
@property (strong, nonatomic) IBOutlet UIView *targetViwe;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // By default, imageViews ignore all touches (pass them the next highest view in the heirarchy
    // so, if we want its gestureRecognizer to actually be able to recognize
    // we need to:
    self.petImageView.userInteractionEnabled = YES;

    self.timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(doSomething) userInfo:nil repeats:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)moveRectangle:(UILongPressGestureRecognizer *)sender {
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
            self.movingView = [[UIView alloc] initWithFrame:sender.view.bounds];
            // if this was an image view, probably want to do something like
            // self.movingView = sender.view.image
            self.movingView.center = [sender locationInView:self.view];
            self.movingView.backgroundColor = [UIColor cyanColor];
            self.movingView.userInteractionEnabled = NO;
            [self.view addSubview:self.movingView];
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            self.movingView.center = [sender locationInView:self.view];
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            // The below function may be useful
//            CGRectIntersectsRect(self.movingView.frame, self.targetViwe.frame);
// alternate approach
//            CGRectContainsPoint(self.targetViwe.frame, self.movingView.center);

            [UIView
             animateWithDuration:1.5
             animations:^{
                 self.movingView.center =
                 CGPointMake(self.movingView.center.x,
                             CGRectGetMaxY(self.view.frame) +
                             CGRectGetHeight(self.movingView.frame));
             }
             completion:^(BOOL finished) {
                 [self.movingView removeFromSuperview];
                 self.movingView = nil;
             }];

        }
            break;
        default:
            break;
    }
}

- (void)doSomething
{
    NSLog(@"Something");
}

@end
